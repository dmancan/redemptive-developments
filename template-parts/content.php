<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rd
 */

?>


		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

		<?php if ( 'post' == get_post_type() ) : ?>
		
			<?php redemptive_developments_posted_on(); ?>
	
		<?php endif; ?>

			
			<?php the_excerpt(); ?>
			

		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'redemptive-developments' ),
				'after'  => '</div>',
			) );
		?>

		<?php //redemptive_developments_entry_footer(); ?>


