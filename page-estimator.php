<?php /* Template Name: Pricing Estimator */ ?>
<?php
/**
 * The template for displaying all testimonials
 *

 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rd
 */

get_header(); ?>

<?php get_template_part( 'ui-parts/ui', 'featureimage' ); ?>

<article class="para">
	<section class="container">
		
		<?php get_template_part( 'ui-parts/ui', 'breadcrumb' ); ?>
		
		<?php while ( have_posts() ) : the_post(); ?>
			<div class="row">
				<div class="col-md-10">
					
					<?php the_title( '<h1>', '</h1>' ); ?>
					<?php if(get_field('sub_title_text')):?>
						<?php echo '<h2>'.get_field('sub_title_text').'</h2>'?>
					<?php endif; ?>
					
					<?php  ADDTOANY_SHARE_SAVE_KIT();  ?>
					
				</div>
			</div>
			
			<style>
				.preload { opacity: 0; position: absolute; top: -9999px; left: -9999px; }
			</style>
			<div class="preload">
				<img src="<?php echo get_template_directory_uri();?>/assets/images/junk1.png" alt="">
				<img src="<?php echo get_template_directory_uri();?>/assets/images/junk2.png" alt="">
				<img src="<?php echo get_template_directory_uri();?>/assets/images/junk3.png" alt="">
				<img src="<?php echo get_template_directory_uri();?>/assets/images/junk4.png" alt="">
				<img src="<?php echo get_template_directory_uri();?>/assets/images/junk5.png" alt="">
				<img src="<?php echo get_template_directory_uri();?>/assets/images/junk6.png" alt="">
				<img src="<?php echo get_template_directory_uri();?>/assets/images/junk7.png" alt="">
			</div><!-- /.preload -->
			
			
			<div class="estimator">
				<div class="row">
					<div class="col-sm-5">
						<div class="box-wrapper hidden-xs">
							<div class="box"></div>
						</div>
					</div>
					<div class="col-sm-7">
						<div class="comparison">
							<div class="reveal" style="background-image: url(<?php echo get_template_directory_uri();?>/assets/images/junk1.png);"></div>
							<img class="junk" src="<?php echo get_template_directory_uri();?>/assets/images/junk1.png">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="ui">
							<ul class="scale">
								<li>Minimum</li>
								<li>1/8</li>
								<li>1/6</li>
								<li>1/4</li>
								<li>1/2</li>
								<li>3/4</li>
								<li>Full</li>
							</ul>
							<input type="range" min="0" max="100" step="5" value="5" class="slider" >
							<ul class="scale">
								<li>$94</li>
								<li>$134</li>
								<li>$174</li>
								<li>$254</li>
								<li>$374</li>
								<li>$474</li>
								<li>$534</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		<hr>
		
		<div class="row">
			<div class="col-sm-8">
				<div class="well estimator">
					<?php the_content();?>
				</div>
			</div>
			<div class="col-sm-4">
				<?php get_sidebar(); ?>
			</div><!-- col -->
		</div><!-- row -->
		
		
		<?php endwhile; // End of the loop. ?>
		
	</section>
	
	<?php get_template_part( 'ui-parts/ui', 'trucks' ); ?>
	
</article>
<?php get_footer(); ?>

 

