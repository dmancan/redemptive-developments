 <?php 
/**
 * Get feature image outside of the loopy.
 *
 */	?>
 
	 
<?php if (is_page_template('page-home.php')): ?>
	
	<?php if(has_post_thumbnail()): ?> 
		<?php
		$thumb_id = get_post_thumbnail_id();
		$thumb_url = wp_get_attachment_image_src($thumb_id,'full', true);
		?>
		<div class="bg header hero" style="background-image: url('<?php echo $thumb_url[0];?>');">
	<?php else: ?>
		<div class="bg header hero" style="background-image: url('<?php echo get_template_directory_uri();?>/assets/images/header.01.png');">
	<?php endif; ?>

	<div class="wrapper">
		<div class="container animated  fadeIn">
			<div class="row">
				<div class="col-xs-10 col-sm-8 col-md-6">
				<?php if (get_field('lead')): ?><h1><?php echo get_field('lead') ?></h1><?php endif ?>
				<?php if (get_field('lead_byline')): ?><h2><?php echo get_field('lead_byline') ?></h2><?php endif ?>
				<?php if (get_field('hero_button_url')): 
				echo '<a href="'. get_field('hero_button_url') .'" class="btn btn-info">'. get_field('hero_button_cta') .'</a>'; endif ?>
				</div>
			</div>
		</div>
	</div>
	
	<?php 
		// ACF repeater field (can have 1...3 overlay images)
		if( have_rows('image_overlay') ): ?>
	 	<?php while( have_rows('image_overlay') ): the_row(); 
		 	
		 	$image = get_sub_field('popup_image');?>
		 	
 			<img src="<?php echo $image['url']; ?>" class="hero-img animated bounceInUp" alt="<?php echo $image['alt']; ?>">
 		<?php endwhile; ?>
 	<?php endif; ?>
</div>


<?php elseif(is_page() || is_single() || is_archive()): ?>
	<?php if(has_post_thumbnail()): ?>
		<?php
			$thumb_id = get_post_thumbnail_id();
			$thumb_url = wp_get_attachment_image_src($thumb_id,'full', true);
		?>
		<div class="bg header" style="background-image: url(<?php echo $thumb_url[0];?>);"></div>
	<?php endif;?>
	
<?php endif;?>
