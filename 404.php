<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package rd
 */

get_header(); ?>

<?php get_template_part( 'ui-parts/ui', 'featureimage' ); ?>
	
	
 <article class="para">
		<section class="container">
		
		<?php get_template_part( 'ui-parts/ui', 'breadcrumb' ); ?>

					<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'redemptive-developments' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'redemptive-developments' ); ?></p>

					<?php get_search_form(); ?>



	</section>

	<?php get_template_part( 'ui-parts/ui', 'trucks' ); ?>
	
</article>	

<?php get_footer(); ?>
