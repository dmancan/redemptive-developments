<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package rd
 */

?>

<!-- 
	################################################################################ 
	Footer
	################################################################################
-->

<footer>
	<section class="container">
		<div class="row">
			<div class="col-sm-4 col-md-3">
				<?php wp_nav_menu( array( 'theme_location' => 'footer', 'items_wrap' => "\n".'<ul class="list-unstyled">'."\n".'%3$s</ul>'."\n", ) ); ?>
			</div>
			<div class="col-sm-4 col-md-3 col-md-offset-3">
				<?php echo get_field('contact_us','options')?>
			</div>
			<div class="col-sm-4 col-md-3">
				<?php echo get_field('booking_cta','options')?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-9">
				
				<?php echo get_field('copyright','options')?>
				
			</div>
			<div class="col-xs-3">
				<br><br><br>
				
				<?php if( have_rows('social_media','options') ): ?>

    			<?php while ( have_rows('social_media','options') ) : the_row(); ?>

  				<a href="<?php echo the_sub_field('social_network_url','options'); ?>" target="_blank" class="rdcons rdbtn"><img src="<?php echo the_sub_field('social_network_icon','options'); ?>" alt="<?php echo the_sub_field('socia_network','options'); ?>"></a>
        			

    			<?php endwhile; ?>

				<?php endif; ?>
				
				
			</div>
		</div>
	</section>
</footer>
</main>
</div> <!-- viewport end -->

<!-- 
	################################################################################ 
	WordPress Footer
	################################################################################
-->

<?php wp_footer(); ?>

</body>
</html>