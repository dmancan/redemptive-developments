<?php
/*
Plugin Name: EUX sub menu
Plugin URI: http://ethicalux.com
Description: display sub menu in sidebar
Author: EUX
Version: 1.0
Author URI: http://ethicalux.com
*/

// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');
	
	
add_action( 'widgets_init', function(){
     register_widget( 'Sub_Menu' );
});	

/**
 * Adds My_Widget widget.
 */
class Sub_Menu extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'Sub_Menu', // Base ID
			__('Sub Menu', 'sub_menu_domain'), // Name
			array( 'description' => __( 'Show current section sub menu', 'sub_menu_domain' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
	
     	echo $args['before_widget'];
     	if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}
		/********************************************************************************/
		global $post;
		
		if($post->post_parent) {
			$children = wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0");
			$title = get_the_title($post->post_parent);
			$link = get_the_permalink($post->post_parent);
		}
		
		else {
			$children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0");
			$title = get_the_title($post->ID);
			$link = get_the_permalink($post->ID);
		}
		if ($children) { ?>
			<ul>
				<li><a href="<?php echo $link; ?>"> <?php echo $title; ?></a></li>
				<?php echo $children; ?>
			</ul>
		
		<?php }

		/********************************************************************************/
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'New title', 'sub_menu_domain' );
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $instance;
	}

} // class
?>