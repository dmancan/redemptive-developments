<?php
/**
 * Add breadcrumbs functionality to your WordPress theme
 *
 * Once you have included the function in your functions.php file
 * you can then place the following anywhere in your theme templates
 * if(function_exists('bootstrap_breadcrumbs')) bootstrap_breadcrumbs();
 *
 * credit to: c.bavota - http://bavotasan.com (thanks for the code start)
 */
function bootstrap_breadcrumbs() {
	echo '<ol class="breadcrumb">';
		echo '<li><a href="'.home_url('/').'">Home</a></li>';

	// are we at "blog home"?
	if(is_home()) {
		echo '<li><a href="#">Blogs</a></li>';
	}

	// where else do we want breadcrumbs
	if(!is_home()) {

		// check if we're in a commerce plugin
		if ( function_exists('is_woocommerce') && is_woocommerce()) {
			echo '<li><a href="/publications/order/">Shop</a></li>';
			$product_cats = wp_get_post_terms(get_the_ID(), 'product_cat');
			echo '<li><a href="/publications/order/' . str_replace(" ", "-", $product_cats[0]->name) .'">' . $product_cats[0]->name . '</a></li>';
		}

		// breadcrumb wordpress structures
		if (is_category() || is_single() || is_single('aof')) {

			if (get_the_category()) {
				$category = get_the_category();
				echo '<li><a href="/blog/category/' . str_replace(" ", "-", $category[0]->cat_name) .'">' . $category[0]->cat_name . '</a></li>';
			}
			if (is_single()) {
				echo '<li class="active">';
					the_title();
				echo '</li>';
			}
		} elseif (is_page()) {
			echo '<li class="active">';
				the_title();
			echo '</li>';
		}

	}
	echo '</ol>';
}
?>
