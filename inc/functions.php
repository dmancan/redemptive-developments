<?php
/**
 * rd functions.
 *
 * @package rd
 */


/**
 * ACF Custom Options
 */

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
	acf_add_options_sub_page('Header');
	acf_add_options_sub_page('Footer');
	acf_add_options_sub_page('Testimonials');
}

/**
	 * Get Random Quote from ACF Options - Testimonials.
	 * Returns: array $rand_row
	 * Access the following in the array:
	 * $rand_row['testimonial_title'], $rand_row['quote'], $rand_row['persons_name'], $rand_row['location_business'], $rand_row['persons_first_name'], $rand_row['background_info'];
	 */
function get_randomquote() {

		
	$rows = get_field('testimonial','options' ); // get all the rows in the ACF Testimonials Options
	
	if (is_array($rows)){
		$rand_row = $rows[ array_rand( $rows ) ]; // get a random row
		
		return $rand_row; // If there's something in the testimonials, return it.
	}
}
/**
*	Removes the Contact 7 Date Widget and replaces it with JQuery Date Picker
*/
add_filter( 'wpcf7_support_html5_fallback', '__return_true' );

?>
