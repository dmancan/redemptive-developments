<?php
/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function redemptive_developments_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'redemptive-developments' ),
		'id'            => 'sidebar',
		'description'   => '',
		'before_widget' => '<aside class="sidebar %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h5>',
		'after_title'   => '</h5>',
	) );
}
add_action( 'widgets_init', 'redemptive_developments_widgets_init' );
?>