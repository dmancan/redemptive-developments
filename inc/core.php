<?php
/**
 * rd core functions.
 *
 *
 * @package rd
 */

if ( ! function_exists( 'redemptive_developments_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function redemptive_developments_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on rd, use a find and replace
	 * to change 'redemptive-developments' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'redemptive-developments', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in two location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'redemptive-developments' ),
		'footer' => esc_html__( 'Footer Menu', 'redemptive-developments' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
/*
	add_theme_support( 'custom-background', apply_filters( 'redemptive_developments_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
*/
}
endif; // redemptive_developments_setup
add_action( 'after_setup_theme', 'redemptive_developments_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function redemptive_developments_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'redemptive_developments_content_width', 640 );
}
add_action( 'after_setup_theme', 'redemptive_developments_content_width', 0 );

/**
 * Enqueue scripts and styles.
 */
function redemptive_developments_scripts() {
	wp_enqueue_style( 'rd-animate', get_template_directory_uri() . '/assets/css/animate.css' );
	wp_enqueue_style( 'rd-style', get_template_directory_uri() . '/assets/css/screen.css' );
	wp_enqueue_script( 'rd-fx', get_template_directory_uri() . '/assets/js/fx.js', array('jquery'), true );
	wp_enqueue_script( 'rd-estimator', get_template_directory_uri() . '/assets/js/estimator.js', array('jquery'), true );
	wp_enqueue_script( 'rd-js', get_template_directory_uri() . '/assets/js/app.js', array('jquery'), true );
	
	/**
	* JESSE: I moved the following script 'estimator.js' (formerly app.js in the prototype website) into this so the estimator slider would work.
	* It seems to be broken in two ways: 1) It doesn't slide the blue overlay over top, and 2) It breaks the primary navigation when it's uncommented.
	**/
	//wp_enqueue_script( 'rd-pr', get_template_directory_uri() . '/assets/js/estimator.js', array('jquery'), true );
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array('jquery'), true );
	//wp_enqueue_script( 'bootstrap-touch', get_template_directory_uri() . '/assets/js/bootstrap-touch/bootstrap-touch-carousel.js', array('jquery'), true );
	//wp_enqueue_style( 'bootstrap-touch', get_template_directory_uri() . '/assets/js/bootstrap-touch/bootstrap-touch-carousel.css' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'redemptive_developments_scripts' );

/**
 * thumbnails/feature images
 */
require get_template_directory() . '/inc/media-settings.php';

/**
 * widgets
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * sidebars (sidebars/footers)
 */
require get_template_directory() . '/inc/sidebars.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


