<?php
/**
 * thumbnails/feature images
 */
 
	add_theme_support('post-thumbnails');
	update_option('thumbnail_size_w', 450);
	update_option('thumbnail_size_h', 450);
	update_option('medium_size_w', 800);
	update_option('medium_size_h', 600);
	update_option('large_size_w', 1680);
	
	// WordPress invited SVG to our party	
	function cc_mime_types($mimes) {
	  $mimes['svg'] = 'image/svg+xml';
	  return $mimes;
	}
	add_filter('upload_mimes', 'cc_mime_types');
?>