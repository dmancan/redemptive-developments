<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rd
 */

get_header(); ?>

<?php get_template_part( 'ui-parts/ui', 'featureimage' ); ?>

<article class="para">
	<section class="container">
		
		<?php get_template_part( 'ui-parts/ui', 'breadcrumb' ); ?>
		
		<?php while ( have_posts() ) : the_post(); ?>
			<div class="row">
				<div class="col-md-8">
					<?php the_title( '<h1>', '</h1>' ); ?>
					
					<?php if(get_field('sub_title_text')):?>
						<?php echo '<h2>'.get_field('sub_title_text').'</h2>'?>
					<?php endif; ?>
					
					<?php ADDTOANY_SHARE_SAVE_KIT() ?>
					<hr>
					
					<?php get_template_part( 'template-parts/content', 'page' ); ?>
					
					
				</div>
				<div class="col-md-4">
					<?php get_sidebar(); ?>
				</div>
			</div>
			
		<?php endwhile; // End of the loop. ?>
		
	</section>
	
	<?php get_template_part( 'ui-parts/ui', 'trucks' ); ?>
	
</article>
<?php get_footer(); ?>
